class ArraySample

  def w arg1 , arg2 , arg3
    %w(arg1 arg2 arg3)
  end

  def W arg1 , arg2 , arg3
    %W(#{arg1} #{arg2}  #{arg3})
  end

  def size arr
    raise 'Not array' unless arr.is_a? Array
    arr.length
  end

end