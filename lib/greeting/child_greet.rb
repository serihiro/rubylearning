# coding: utf-8

require File.expand_path('../parent_greet.rb', __FILE__) 

class ChildGreet < ParentGreet
  # override
  def greet name
    super << "hoge!"
  end

  def longer_length? compare_length
    raise "no args are set" if compare_length.nil?
    raise "length is not set" if length.nil?
    result = compare_length > length ? true : false
  end


end

