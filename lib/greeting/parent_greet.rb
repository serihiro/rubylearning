# coding: utf-8

class ParentGreet

  attr_accessor :length

  def greet name
    raise "No args are set" if name.nil? || name.empty?
    
    return "hello " << name.to_s if name.instance_of?(String) || name.instance_of?(Array) && name.length() == 1
    return "hello " << name.join(',') if name.instance_of?(Array) && name.length() >= 2
  end

  def keirei
    '∠(｀・ω・´)'
  end

  def maid name
    raise "No args are set" if name.nil? || name.empty?
    "お帰りなさいませ" << name << "様!"
  end
end
