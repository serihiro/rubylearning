class HashSample

  def three_attribute key1,value1,key2,value2,key3,value3
    {key1.to_sym => value1,key2.to_sym => value2,key3.to_sym => value3}
  end

  def mugen_element **mugen_hash
    mugen_hash.to_s
  end

end
