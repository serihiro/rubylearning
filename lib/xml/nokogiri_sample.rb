# coding: utf-8

class NokogiriSample
  require 'nokogiri'
  
  def initialize xml
=begin
    xml = <<-EOM
      <?xml version="1.0" encoding="UTF-8"?>
	<items>
	  <item id="123">Andy</item>
	  <item id="234">Brian</item>
	  <item id="345">Charles</item>
	</items>
    EOM
=end
    @doc = Nokogiri::XML(xml)
  end

  def get_element_by_xpath xpath
    @doc.xpath(xpath)
  end

  def get_elenent_by_css_selector selector
    @doc.css(selector)
  end
end
