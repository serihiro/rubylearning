# coding: utf-8

require 'spec_helper'
require File.expand_path('../../lib/greeting/child_greet.rb', __FILE__)

describe ChildGreet ,'runs occury' do
  
  let(:instance){ ChildGreet.new }
  it 'should return string' do
    expect(instance.greet('serizawa')).to eq 'hello serizawahoge!'
  end

  context '#longer_length' do
    let(:arg){ nil }
    subject do
      instance.length = 3
      instance.longer_length?(length)
    end

    context 'args is 1' do
      let(:length){ 1 }
      it do
        expect(subject).to eq false
      end
    end

    context 'args is 5' do
      let(:length){ 5 }
      it do
        expect(subject).to eq true
      end
    end

    it 'shoud raise when arg is not set' do
      expect { subject }.to raise_error
    end

    it 'shoud raise when length is not set' do
      expect { instance.longer_length?(3) }.to raise_error
    end
  end
  
end
