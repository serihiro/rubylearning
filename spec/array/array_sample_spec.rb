# coding: utf-8

require 'spec_helper'
require File.expand_path('../../../lib/array/array_sample.rb', __FILE__)

describe ArraySample do
  let(:instance){ ArraySample.new }

  it 'return staticR array' do
    expect(instance.w 'a','b','c').to eq(['arg1','arg2','arg3'])
  end

  it 'return dynamic array' do
    expect(instance.W 'a','b','c').to eq(['a','b','c'])
  end

  context '#size' do
    let(:arg){ %w(a, b, c) }
    subject{ instance.size(arg) }
    context 'when args is Array' do
      it  do
        expect(subject).to eq(3)
      end

      it do
        expect{subject}.not_to raise_error
      end
    end

    context 'when args is Strjng' do
      let(:arg){ 'String' }
      it do
        expect{subject}.to raise_error
      end
    end
  end
end
