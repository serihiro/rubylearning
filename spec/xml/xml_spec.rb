# coding: utf-8

require 'spec_helper'
require File.expand_path('../../../lib/xml/nokogiri_sample.rb', __FILE__)

describe NokogiriSample , 'parse xml' do

  let(:xml) do
    <<-EOM
          <?xml version="1.0" encoding="UTF-8"?>
	    <items>
        <item id="123">Andy</item>
	      <item id="234">Brian</item>
	      <item id="345">Charles</item>
	    </items>
    EOM
  end
  let(:instance){ NokogiriSample.new xml }
  let(:xpath){''}
  subject{ instance.get_element_by_xpath(xpath) }


  context 'should return all item' do
    let(:xpath){ '/items' }
    it do
      expect(subject).not_to eq(nil)
      expect(subject.text).to eq("\n        Andy\n\t      Brian\n\t      Charles\n\t    ")
    end
  end

  context 'should return Enumrable object' do
    let(:xpath){ '/items/item' }
    it do
      expect(subject).not_to eq(nil)
      expect(subject.is_a?(Enumerable)).to eq(true)
      expect(subject.text).to eq('AndyBrianCharles')
    end
  end

  context 'should get value of "Andy" , "Brian" and "Charles"' do
    let(:xpath){ '/items/item' }
    it do
      subject.each_with_index do |item,index|
        case index
          when 0
            expect(item.content).to eq 'Andy'
          when 1
            expect(item.content).to eq 'Brian'
          when 2
            expect(item.content).to eq 'Charles'
        end
      end
    end
  end
end
