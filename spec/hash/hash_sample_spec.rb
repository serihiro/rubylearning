# coding: utf-8

require 'spec_helper'
require File.expand_path('../../../lib/hash/hash_sample.rb', __FILE__)

describe HashSample do

  before :all do
    @instance = HashSample.new
  end

  context '#three_attribute' do
    it 'returns hash object with 3 attributes'  do
      expect(@instance.three_attribute 'hoge','foo','zoo','bar','fuga','piyo').to eq({hoge:"foo",zoo:'bar',fuga:'piyo'})
    end
  end

  shared_examples_for 'returns args.to_s' do
    let(:arg){ {} }
    subject{ @instance.mugen_element(arg) }
    it { expect(subject).to eq arg.to_s }
  end

  context '#mugen_element' do
    it_should_behave_like 'returns args.to_s' do
      let(:arg){ {madoka: 'uxehihihi', homura: 'madokaxa'} }
    end

    it_should_behave_like 'returns args.to_s' do
      let(:arg){ {a: '1', b:'2', c:'3'} }
    end
  end
end
