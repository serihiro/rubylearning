# coding: utf-8

require 'spec_helper'
require File.expand_path('../../lib/greeting/parent_greet.rb', __FILE__)

describe ParentGreet , 'greet me' do
  let(:instance){ ParentGreet.new }

  context '#greet' do
    let(:arg){ '' }
    subject{ instance.greet(arg)}
    context 'should greet to serizawa' do
      let(:arg){ 'serizawa' }
      it do
        expect(subject).to eq 'hello serizawa'
      end
    end

    context 'should greet to several people' do
      let(:arg){ ['serizawa','tanaka','yamada'] }
      it do
        expect(subject).to eq 'hello serizawa,tanaka,yamada'
      end
    end

    context 'should raise exception when name is nil' do
      let(:arg){ nil }
      it do
        expect{ subject }.to raise_error(RuntimeError)
      end
    end
  end

  it 'should do keirei' do
    expect(instance.keirei).to eq '∠(｀・ω・´)'
  end

  it 'should say okaerinasaimase!' do
    expect(instance.maid('serizawa')).to eq 'お帰りなさいませserizawa様!'
  end

end
